#  - 2018-07-22 00:00:00

* SPECIÁLNÍ NABÍDKA - 
* 0,3l Mexická polévka z červené fazole s grilovaným Chorizem a sýrovými nachos - **55 Kč**
* 200g Filet candáta z grilu podávaný s žampionem Portobello /plněným zeleninovým ratatouille, zapékaný mozzarellou/, bylinkovým dipem a pařížskými brambůrkami restovanými na olivovém oleji a rozmarýnu - 295 Kč
* 200g Krůtí steak /marinovaný v řeckém jogurtu/ podávaný s grilovaným čerstvým ananasem, šťouchanými smetanovými bramborami a avokádovým quacamolle - 265 Kč
* 200g Hovězí flank steak z grilu /filovaný/ podávaný s ragů z čerstvých hub Šitake, americkými bramborami a listovým salátem - 315 Kč
* 250g Italské bramborové gnocchi v jemné omáčce z vyzrálého chedaru s grilovanými kuřecími prsíčky, cherry rajčátky a parmezánem - 195 Kč
* 350g Grilovaný kozí sýr na italském listovém salátu s jemným vinaigrette dresinkem, marinovanou červenou řepou, vlašskými oříšky a granátovým jablkem, podávaný s parmezánovou focacciou - 215 Kč
* 200g Grilovaná kotleta podávaná s kornoutem tyrolské sušené šunky a smaženými cibulovými kroužky, bramborovými kroketami a domácí BBQ omáčkou - 255 Kč
* Pizza Quattro Salame /tomatové sugo, mozzarella, italská Ventricina, česnekový salám Felino, italská klobáska, sušená šunka, kukuřice, smažená cibulka/ - 195 Kč
* Krásnou neděli Vám přeje Suzies  - 

