#!/usr/bin/env python3

import pprint
from datetime import datetime
import mdmail
import argparse

from restaurants import FatFree,Sodexo, Goa, SlatinaBistro, Bastirna, TuranskaBeseda, ColaTransport

def daily_to_md(daily_menu):
  """Converts daily_menu dictionary to a markdown formatted menu
  """
  md = "## {name}\n\n".format(**daily_menu)
  for dish in daily_menu['dishes']:
      md += "* {name} - {price}\n".format(**dish)
  
  md += '\n'
  return md


parser = argparse.ArgumentParser(description="Fetch menus for restaurants and generate markdown menu. Optionally send over mail")
parser.add_argument('--key', required=True, help='Zomato API key')
parser.add_argument('--smtp', help="SMTP server for sending emails")
parser.add_argument('--recipients', help='File with mail recipients')
args = parser.parse_args()

smtp_host = args.smtp
recipient_file = args.recipients

restaurants = [ 
  Sodexo(),
  FatFree(),
  SlatinaBistro(),
  Bastirna(),
  Goa(),
  TuranskaBeseda(),
  ColaTransport(args.key)  ] 
for_date = datetime.today()

output = "# Denni menu - {}\n".format(for_date.strftime("%d.%m.%Y"))
for rest in restaurants:
    try:
      output += daily_to_md(rest.get_daily_menu(for_date=for_date))
    except Exception as e:
      print(e) 
      pass

print(output)


if smtp_host and recipient_file:
  recipients = []
  with open(recipient_file, 'rt') as fd:
    for email in fd:
      recipients.append(email.strip())

  smtp = {
    'host': smtp_host,
    'port': 25,
    'tls': False,
    'ssl': False,
    'user': '',
    'password': '',
  }
  
  mdmail.send(output, subject="Denni menu - {}".format(for_date.strftime("%d.%m.%Y")),
            from_email='obedy@slatina.cz', to_email=recipients, smtp=smtp)
  print("Email sent out to: {}".format(recipients))
