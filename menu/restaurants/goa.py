from bs4 import BeautifulSoup
from datetime import datetime
import requests

from .restaurant import Restaurant

class Goa(Restaurant):
    menu_url = 'http://www.restaurant-goa-slatina.cz/lang-en/denni-menu'
    restaurant_name = "Goa Slatina"

    def __init__(self):
        super().__init__()

    def get_daily_menu(self, for_date, from_file=None):
        if from_file:
            with open(from_file) as fd:
               html = fd.read()
        else:
            r = requests.get(self.menu_url)
            html = r.text

        dishes = []
        menu = {
            "name": self.restaurant_name,
            "date": for_date.strftime("%d.%m.%Y"),
            "dishes": dishes
        }

        day = for_date.strftime("%A")
        soup = BeautifulSoup(html, "html.parser")
        tag = soup.find("h2", string=day)
        try:
            tag = tag.nextSibling
        except AttributeError:
            print("{}: No menu available for {}".format(self.restaurant_name, day))
            return menu
        
        while tag != None and tag.name != 'h2':
            if tag.name != 'br':
                name = str(tag)
                price = ""
                dish = { "name": name, "price": price }
                dishes.append(dish)

            tag = tag.nextSibling

        return menu


if __name__ == '__main__':
    goa = Goa()
    test_date = datetime(2019, 9, 25)
    print(goa.get_daily_menu(test_date, from_file='test/goa.html'))
    print(goa.get_daily_menu(for_date=datetime.today()))
