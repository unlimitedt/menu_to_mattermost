from bs4 import BeautifulSoup
from datetime import datetime
import requests

from .restaurant import Restaurant

class TuranskaBeseda(Restaurant):
    menu_url = 'https://www.turanskabeseda.cz/menu/'
    restaurant_name = "Turanska Beseda"

    def __init__(self):
        super().__init__()

    def get_daily_menu(self, for_date=None, from_file=None):
        if from_file:
            with open(from_file) as fd:
                html = fd.read()
        else:
            r = requests.get(self.menu_url)
            html = r.text

        dishes = []
        menu = {
            "name": self.restaurant_name,
            "date": for_date.strftime("%d.%m.%Y"),
            "dishes": dishes
        }
        
        soup = BeautifulSoup(html, "html.parser")
        menu_div = soup.find('div', attrs={'id': "obedova-nabidka"})
        days = menu_div.findAll('div', attrs={ 'class': "intro" })
        for day in days:
            menu_date = day.find('div', attrs={'class': 'day'}).text.strip()
            day_name, day_date = menu_date.split(' ', 1)

            if for_date.date() != datetime.strptime(day_date, "%d. %m. %Y").date():
                continue
            
            if day.next_sibling:
                for item in day.next_sibling.findAll('div', attrs={'class': 'item'}):
                    name = item.select('div[class="name"]')[0].text
                    price = item.select('div[class="price"]')[0].text
                    dish = { "name": name, "price": price }
                    dishes.append(dish)
        

        

                
        return menu


if __name__ == "__main__":
    beseda = TuranskaBeseda()
    
    print(beseda.get_daily_menu(datetime(2019, 10, 3), "test/beseda.html"))
    print(beseda.get_daily_menu(datetime.today()))