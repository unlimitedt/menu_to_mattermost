from .zomato_restaurant import ZomatoRestaurant

from datetime import datetime

class ColaTransport(ZomatoRestaurant):
    restaurant_id = '16506756'
    restaurant_name = "Cola transport"
    def __init__(self, API_KEY):
        if (API_KEY == None):
            raise ValueError("Missing API key for use with Zomato")

        super().__init__(API_KEY, self.restaurant_id)

if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description="Fetch menu from Cola Transport restaurant")
    parser.add_argument('--key', required=True, help='Zomato API key')
    args = parser.parse_args()

    cola = ColaTransport(args.key)
    print(cola.get_daily_menu(datetime.today()))