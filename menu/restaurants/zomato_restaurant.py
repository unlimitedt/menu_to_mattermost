from .restaurant import Restaurant
from pyzomato import Pyzomato

class ZomatoRestaurant(Restaurant):
    def __init__(self, API_KEY, restaurant_id):
        self.restaurant_id = restaurant_id
        self.api = Pyzomato(API_KEY)

    def get_daily_menu(self, for_date=None):
        menu = self.api.getDailyMenu(self.restaurant_id)
        dishes = zomato_to_custom(menu)
        daily_menu = { 
            'name': self.restaurant_name,
            'date': for_date.strftime("%d.%m.%Y"),
            'dishes': dishes }
        return daily_menu	
        
def zomato_to_custom(zom):
    custom_dishes = []
    for dish in zom['daily_menus'][0]['daily_menu']['dishes']:
        new_dish = { 'name': dish['dish']['name'].strip(), 'price': dish['dish']['price'] }
        custom_dishes.append(new_dish)
  
    return custom_dishes	
