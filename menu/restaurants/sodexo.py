from bs4 import BeautifulSoup
from datetime import datetime
import requests

from .restaurant import Restaurant

class Sodexo(Restaurant):
    menu_url = 'http://honeywell-brno-3.portal.sodexo.cz/cs/uvod'
    restaurant_name = "Sodexo"

    def __init__(self):
        super().__init__()

    def get_daily_menu(self, for_date=None, from_file=None):
        if from_file:
            with open(from_file) as fd:
               html = fd.read()
        else:
            r = requests.get(self.menu_url)
            html = r.text
        dishes = []
        menu = {
            "name": self.restaurant_name,
            "date": for_date.strftime("%d.%m.%Y"),
            "dishes": dishes
        }
       
        soup = BeautifulSoup(html, "html.parser")
        for line in soup.find_all('tr'):
            if len(line.select('td[class="popis"]')) == 0:
                continue
            name = line.select('td[class="popisJidla"]')[0].text
            price = line.select('td[class="pismo"]')[0].text
            dish = { "name": name, "price": price }
            dishes.append(dish)
            
        return menu


if __name__ == '__main__':
    sodexo = Sodexo()

    print(sodexo.get_daily_menu(datetime.today(), from_file="test/sodexo.html"))
    print(sodexo.get_daily_menu(datetime.today()))
