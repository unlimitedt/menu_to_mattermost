from datetime import datetime
import json
import requests

from .restaurant import Restaurant

class Bastirna(Restaurant):
    menu_url = 'https://jidelna.bastirnaslatina.cz/scripts/dnesni_menu.php'
    restaurant_name = "Bastirna"

    def __init__(self):
        super().__init__()

    def _extract_dish(self, data):
        name="aa"
        price=""
        
        label = list(data.keys())[0]
        main_data = data[label]["components"]["main_components"]
        main = main_data[list(main_data.keys())[0]]["comp_name"]
        side_dish = ""
        try:
            side_dish_data = data[label]["components"]["side_components"]
            side_dish = side_dish_data[list(side_dish_data.keys())[0]]["comp_name"]
        except:
            pass
        name = "{} - {}, {}".format(label, main, side_dish)
        dish = { "name": name, "price": price }
        return dish


    def get_daily_menu(self, for_date=None, from_file=None):
        if from_file:
            with open(from_file) as fd:
                data = json.load(fd)
        else:
            params = {'readFoods': 'true', 'week': 'this', 'group': '16'}
            r = requests.get(self.menu_url, params=params)
            data = r.json()

        dishes = []
        menu = {
            "name": self.restaurant_name,
            "date": for_date.strftime("%d.%m.%Y"),
            "dishes": dishes
        }
        
        for day in data["menuWeek"]:
            if for_date.strptime(day["date"], "%Y-%m-%d").date() == for_date.date():
                for food in day["foods"]:
                    info = json.loads(food["food_in_menu_info"]["label_data"])
                    dishes.append(self._extract_dish(info))

        return menu


if __name__ == "__main__":
    bastirna = Bastirna()
    for_date = datetime(2019, 9, 30)
    print(bastirna.get_daily_menu(for_date, from_file="test/bastirna.json"))
    print(bastirna.get_daily_menu(datetime.today()))