from .fat_free import FatFree
from .sodexo import Sodexo
from .goa import Goa
from .slatina_bistro import SlatinaBistro
from .bastirna import Bastirna
from .beseda import TuranskaBeseda
from .cola_transport import ColaTransport
