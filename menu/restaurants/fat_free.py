from bs4 import BeautifulSoup
from datetime import datetime
import requests

from .restaurant import Restaurant

class FatFree(Restaurant):
    menu_url = 'http://www.fat-free.cz/menu/'
    restaurant_name = "Fat Free"

    def __init__(self):
        super().__init__()

    def get_daily_menu(self, for_date=None, from_file=None):
        if from_file:
            with open(from_file) as fd:
                html = fd.read()
        else:
            r = requests.get(self.menu_url)
            html = r.text

        dishes = []
        menu = {
            "name": self.restaurant_name,
            "date": for_date.strftime("%d.%m.%Y"),
            "dishes": dishes
        }
        
    
        try:
            soup = BeautifulSoup(html, "html.parser")
            for line in soup.table.find_all('tr'):
                data = line.find_all('td')
                name = data[1].contents[0]
                price = data[2].contents[0]
                dish = { "name": name, "price": price }
                dishes.append(dish)
        except IndexError:
            print("{}: No menu available for {}".format(self.restaurant_name, for_date.strftime("%d.%m.%Y")))
            return menu
                
        return menu


if __name__ == "__main__":
    ff = FatFree()
    
    print(ff.get_daily_menu(datetime.today(), "test/fat-free.html"))
    print(ff.get_daily_menu(datetime.today()))