from bs4 import BeautifulSoup
from datetime import datetime
import requests

from .restaurant import Restaurant

class SlatinaBistro(Restaurant):
    menu_url = 'https://www.slatinabistro.cz/obedy'
    restaurant_name = "Slatina Bistro"

    def __init__(self):
        super().__init__()

    def get_daily_menu(self, for_date=None, from_file=None):
        
        if from_file:
            with open(from_file) as fd:
                html = fd.read()
        else:
            r = requests.get(self.menu_url)
            html = r.text
    
        dishes = []
        menu = {
            "name": self.restaurant_name,
            "date": for_date.strftime("%d.%m.%Y"),
            "dishes": dishes
        }
    
        soup = BeautifulSoup(html, "html.parser")
        days = soup.findAll(attrs={ 'class': "col-12 col-lg-6 text-center menu-col" })
        for day in days:
            menu_date = day.find('p', attrs={'class': 'date'}).text.strip()
            
            if datetime.strptime(menu_date, '%d.%m.%Y').date() != for_date.date():
                continue
            
            meals = day.findAll(attrs={'class': ['col-12 col-sm-9 col-lg-10 name main', 'col-12 col-sm-9 col-lg-10 name']})
            for meal in meals:
                for col in meal.children:
                    if col.name != 'span':
                        name = col.strip()
                        price = col.parent.next_sibling.text.strip()

                        dish = { "name": name, "price": price }
                        dishes.append(dish)            
            
        return menu

if __name__ == "__main__":
    bistro = SlatinaBistro()
    for_date = datetime(2019, 9, 26)
    print(bistro.get_daily_menu(for_date, "test/bistro.html"))
    print(bistro.get_daily_menu(datetime.today()))