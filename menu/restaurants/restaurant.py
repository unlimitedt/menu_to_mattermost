from abc import ABC, abstractmethod

class Restaurant(ABC):
    def __init__(self, API_KEY=None):
        pass

    @abstractmethod
    def get_daily_menu(self, for_date=None):
        """Returns dictionary containing the daily menu of the restaurant
        """
        raise NotImplementedError